# Grand Debat analyze

These notebooks analyze french grand debat result

## Context
Due to gillet jaune riot, Macron decided to open a national opinion way about french society.
After some month available, this opinion way is now close and result is available.

`data` folder store these results fetched [here](https://granddebat.fr/pages/donnees-ouvertes).
You have also french demography city by city splitted by INSEE Code and a mapping table to INSEE code and postal code used for the grand debat.

## Analyze
- `Analyze-Population.ipynb` analyze result by city small from less than 2000 residents (village) to upper 200,000 (metropole).
Notebook is exported to html at https://fjolain.gitlab.io.grand-debat
